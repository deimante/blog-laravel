<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function posts()
    {
        return view('pages.naujas');
    }
    public function saugok()
    {
        $this->validate(request(),
            ['title'=>'name',
                'body'=>'text',
                'body'=>'category']);
        Post::create(request()->all());
        return redirect('/');
    }

    public function edit (Post $post)
    {
        $this->validate(request(),
            ['title'=>'name',
                'body'=>'text']);
        return view('pages.edit', compact('post'));
    }
    public function update(Request $request, Post $post)
    {
        Post::where('id',$post->id )->update($request->only(['name', 'text']));
        return redirect('/');
    }

    public function delete(Post $post)
    {
        $post->delete();
        return redirect('/dashboard');
    }
    public function dash()
    {
        $posts=Post::all();
        $data=Post::paginate(6);
        return view('pages.dashboard', compact('posts', 'data'));
    }

}