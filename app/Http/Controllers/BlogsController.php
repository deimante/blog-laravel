<?php

namespace App\Http\Controllers;

use App\Blogs;

class BlogsController extends Controller
{
    public function blogs()
    {

        return view('pages.naujas');
    }

    public function home()
    {
        return view('home');
    }
    public function saugok()
    {$this->validate(request(),
        [
            'name'=>'required',
            'text'=>'required'
        ]);

        Posts::create(request()->all());
        return view('pages.home');

    }
    public function rodyt()
    {
        $posts=Blogs::all();
        return compact('posts');
    }

}