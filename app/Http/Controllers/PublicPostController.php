<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PublicPostController extends Controller
{
    public function rodyt()
    {
        $posts=Post::paginate(6);
        return view('pages.home', compact('posts'));
    }
    public function rodytvisa(Post $post)
    {
        return view('pages.full', compact('post'));
    }
}