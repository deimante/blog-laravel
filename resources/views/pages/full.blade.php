@extends('layouts/main')
@section('content')
    <h2>{{$post->name}}</h2>
    <p>{{$post->text}}</p>
<p>Kategorija: {{$post->category}}</p>

    @auth
        <p><a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Redaguoti</a></p>
        <p><a class="btn btn-default" id="tr" href="/post/{{$post->id}}/delete" role="button">Trinti</a></p>
    @endauth


    <div class="comments">
        <h2>Komentarai</h2>
        <ul>
        @foreach($post->comments as $comment)
            <li>{{$comment->body}}</li>
            <hr>
            @endforeach
        </ul>
    </div>

    <div class="content">
        <div class="title m-b-md">
            <h2>Komentarai</h2>
        </div><br>

        <form action="/posts/{{$post->id}}/comments" method="post" class="form-horizontal" method="post">
            {{csrf_field()}}

            <div class="form-group">
                <label for="title">Vardas</label>
                <input type="title" class="form-control" id="title" name="title" placeholder="Vardas">
            </div>
            <div class="form-group">
                <label for="body">Komentaras</label>
                <input type="body" class="form-control" id="body" name="body" placeholder="Komentaras">
            </div>
            <button type="submit" name="submit" value="submit" class="btb btn-primary">Siųsti</button>

        </form>
    </div>



@endsection