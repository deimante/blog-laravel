@extends('layouts/main')
@section('content')

    @include('includes/errors')
    <div class="content">
        <div class="title m-b-md">
            <h2>Naujas įrašas</h2>
        </div><br>

        <form action="/irasai" method="post" class="form-horizontal">
            {{csrf_field()}}

            <div class="form-group">
                <label for="category">Kategorija</label>
                <select class="form-control" id="category" name="category">
                    <option>Kompiuteriai</option>
                    <option>Telefonai</option>
                    <option>Priedai</option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Pavadinimas</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Pavadinimas">
            </div>
            <div class="form-group">
                <label for="text">Prekės komentaras</label>
                <input type="text" class="form-control" id="text" name="text" placeholder="Prekės komentaras">
            </div>
            <button type="submit" name="submit" value="submit" class="btb btn-primary">Pridėti</button>

</form>
    </div>




@endsection

