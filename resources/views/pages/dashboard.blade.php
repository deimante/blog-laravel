@extends('layouts/main')
@section('content')

    <table>
        <tr>
            <th>Pavadinimas</th>
            <th>Prekės aprašymas</th>

        </tr>
        @foreach($posts as $post)

            <tr>
                <td><h3>{{str_limit($post->name,20)}}</h3></td>
                <td><p>{{str_limit($post->text,50)}} </p></td>
                <td><p><a class="btn btn-default" href="post/{{$post->id}}" role="button">Rodyti daugiau...</a></p></td>
                <td><p><a class="btn btn-default" href="/post/{{$post->id}}/edit" role="button">Redaguoti</a></p></td>
                <td><p><a class="btn btn-danger" href="/post/{{$post->id}}/delete" role="button">Šalinti</a></p></td>
            </tr>
            <tr>

        @endforeach
    </table>
    <p><a class="btn btn-danger"  href="/nauji_irasai">Naujas įrašas</a></p>

    <div>
        {{$data->links()}}
    </div>

@endsection