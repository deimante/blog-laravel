@extends('layouts/main')
@section('content')

    @include('includes/errors')
    <div class="content">
        <div class="title m-b-md">

            <h2>Nauji įrašai</h2>
        </div>
        <form action="/post/{{$post->id}}" method="post" class="form-horizontal">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <label for="name">Pavadinimas</label>
            <input type="text" name="name" id="name" value="{{$post->name}}">
            <label for="text">Prekės komentaras</label>
            <input type="text" name="text" id="text" value="{{$post->text}}">
            <label for="text">Prekės kategorija</label>
            <input type="text" name="category" id="category" value="{{$post->category}}">
            <button type="submit" name="submit" value="submit">Pridėti</button>
        </form>
    </div>

@endsection