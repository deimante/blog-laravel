@extends('layouts/main')
@section('content')

    @foreach($posts as $post)

        <div class="col-md-4">
            <h2>{{$post->name}}</h2>
            <p>{{str_limit($post->text,150)}} </p>
            <p>{{str_limit($post->category,150)}} </p>

            <p><a class="btn btn-default" href="post/{{$post->id}}" role="button">Skaityti daugiau...</a></p>
        </div>
    @endforeach

    <div >
        {{$posts->links()}}
    </div>





@endsection